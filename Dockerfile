#stage1 : Compile et Build angular code source
#image de base docker
FROM node:lts-slim as build
#definir le répertoire de travail d'un conteneur pour l'exection de partie web
WORKDIR /app
#copier le fichier qui contient le projet dans la racine
COPY . .
#telecharger un package et ses dépendances
RUN npm install
#exécute le champ de construction à partir du package.json
RUN npm run build
#stage 2 : Serve app avec nginx server
#image de base docker
FROM nginx:stable
#Copiez la sortie de construction pour remplacer le contenu nginx par défaut
COPY --from=build /app/dist/laboratoire /usr/share/nginx/html

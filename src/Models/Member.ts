export interface Member {
  id : string;
  cin : string,
  nom:string|null,
  createddate : Date,
  cv : string,
  type:string,
  userPassword :string | null,
  role : string
}

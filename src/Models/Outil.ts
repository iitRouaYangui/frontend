import {DatePipe} from "@angular/common";
import {Member} from "./Member";

export interface Outil {
  id : string ,
  idMembre:string|null,
  source:string,
  date : Date,
  sourceName:string,
  membreDTO:Member
}

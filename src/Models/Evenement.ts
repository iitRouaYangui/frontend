import {Member} from "./Member";

export interface Evenement{
  id: string,
  titre:string,
  date: Date,
  lieu:string,
  idMembre : string | null,
  membreDTO: Member
}

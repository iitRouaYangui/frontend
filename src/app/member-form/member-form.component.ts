import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormArray,
  FormControl,
  Validators
} from "@angular/forms";
import {MemberService} from "../../Services/member.service";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {Member} from "../../Models/Member";
import {HttpClient} from "@angular/common/http";
interface role {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-member-form',
  templateUrl: './member-form.component.html',
  styleUrls: ['./member-form.component.css']
})
export class MemberFormComponent implements OnInit {
  form: any;
  cin: any;
  name: any;
  type: any;
  cv: any;
  currentId: any;
  item1: any;
  liste : any;
  member :any;
  @ViewChild('fileInput') fileInput!: ElementRef;
  fileAttr = 'Choose your CV';
  pdfFile: any;
  pdfSrc: any;
  pdfBufferRender: any;
  localPDF:any;
  roles: role[] = [
    {value: 'Teacher', viewValue: 'Teacher'},
    {value: 'Student', viewValue: 'Student'},
  ];
  initForm(item:any): void {
      this.form = new FormGroup({
      cin: new FormControl(item?.cin, [Validators.required , Validators.minLength(8) , Validators.maxLength(9)]),
      nom: new FormControl(item?.nom, [Validators.required]),
      cv: new FormControl(null,[Validators.required] ),
      role: new FormControl(item?.role, [Validators.required]),
      userPassword: new FormControl(item?.userPassword, [Validators.required]),
    })
  }

  constructor(private MemberService: MemberService, private router: Router , private activatedRoute:ActivatedRoute,  private http: HttpClient ,private route: Router) {

  }

  uploadFileEvt(imgFile: any) {
    const pdfTatget: any = imgFile.target;
    this.fileAttr = '';
    Array.from(imgFile.target.files).forEach((file: any) => {
      this.fileAttr= file.name;
    });
    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.pdfSrc = e.target.result;
        this.localPDF = this.pdfSrc;
        console.log( this.pdfSrc);
      };
      this.pdfBufferRender = pdfTatget.files[0];
      reader.readAsDataURL(pdfTatget.files[0]) ;
    }
  }
  ONSUB(): void {
    const MemberToSave: Member = {...this.item1, ...this.form.value};
    //const MemberToSave = this.form.value;
    MemberToSave.cv=this.localPDF;
    this.MemberService.saveMember(MemberToSave);
  }
  ngOnInit(): void {
    this.currentId=this.activatedRoute.snapshot.params['id'];
    if (!!this.currentId) {
      console.log(this.currentId);
      this.MemberService.getMemberbyId(this.currentId).then((item: Member) => {
        this.item1 = item;
        console.log(this.item1);
        this.initForm(this.item1)
      });
      console.log('finish');
    }else {
      this.initForm(null);
    }


  }

  // add() {
  //   this.http.post('http://localhost:9000/labo/api/addMember', this.member).subscribe(data => {
  //     console.log(data);
  //     this.route.navigate(['members']) ;
  //   }, error => {
  //     console.log(error.message);
  //   }) ;
  // }

}

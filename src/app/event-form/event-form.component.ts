import { Component, OnInit } from '@angular/core';
import {Article} from "../../Models/Article";
import {ActivatedRoute, Router} from "@angular/router";
import {DatePipe} from "@angular/common";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {EventService} from "../../Services/event.service";
import {Evenement} from "../../Models/Evenement";

@Component({
  selector: 'app-event-form',
  templateUrl: './event-form.component.html',
  styleUrls: ['./event-form.component.css']
})
export class EventFormComponent implements OnInit {
  formEvent: any;
  item1:any;
  currentId:any;
  dateMaxDate = new Date();

  constructor(private eventService: EventService, private router: Router , private activatedRoute:ActivatedRoute,public datepipe: DatePipe) {
  }
  initForm(item:any): void {
    if(item?.date!=null){

      this.formEvent = new FormGroup({
        titre: new FormControl(item?.titre, [Validators.required]),
        lieu: new FormControl(item?.lieu,[Validators.required] ),
        date: new FormControl(item?.date, [Validators.required]),
      })
    }
    else
    {
      this.formEvent = new FormGroup({
        titre: new FormControl(null, [Validators.required]),
        lieu: new FormControl(null,[Validators.required] ),
        date: new FormControl(new Date(), [Validators.required]),
      })
    }

  }

  ONSUB(): void {
    const eventTosave : Evenement  ={...this.item1 , ...this.formEvent.value};

    this.eventService.saveEvent(eventTosave);
  }


  ngOnInit(): void {
    this.currentId=this.activatedRoute.snapshot.params['id'];
    if (!!this.currentId) {
      console.log(this.currentId);
      this.eventService.getEventyId(this.currentId).then((item: Evenement) => {
        this.item1 = item;
        this.initForm(this.item1)
      });
      console.log('finish');
    }else {
      this.initForm(null);
    }
  }
}

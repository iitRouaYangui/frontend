import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {MemberListComponent} from "./member-list/member-list.component";
import {MemberFormComponent} from "./member-form/member-form.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {ToolsListComponent} from "./tools-list/tools-list.component";
import {ToolsFormComponent} from "./tools-form/tools-form.component";
import {ArticleListComponent} from "./article-list/article-list.component";
import {ArticleFormComponent} from "./article-form/article-form.component";
import {EventListComponent} from "./event-list/event-list.component";
import {EventFormComponent} from "./event-form/event-form.component";
import {ForbiddenComponent} from "./forbidden/forbidden.component";
import {MemberGuard} from "./member.guard";
import {LogInComponent} from "./log-in/log-in.component";

const routes : Routes =[
  {
    path:'',
    pathMatch:'full',
    redirectTo:'Dashboard',
  },
  {
    path:'members',
    pathMatch:'full',
    component:MemberListComponent,
  },
  {
    path:'ADD',
    pathMatch:'full',
    component:MemberFormComponent,
    canActivate:[MemberGuard]
  },

  {
    path:'members/:id/edit',
    pathMatch:'full',
    component: MemberFormComponent
  },
  {
    path:'Dashboard',
    pathMatch:'full',
    component:DashboardComponent,
  }
  ,
  {
    path:'Tools',
    pathMatch:'full',
    component:ToolsListComponent,
  }
  ,
  {
    path:'AddTools',
    pathMatch:'full',
    component:ToolsFormComponent,
  },

  {
    path:'Tools/:id/edit',
    pathMatch:'full',
    component: ToolsFormComponent,
  }
  ,
  {
    path:'article',
    pathMatch:'full',
    component:ArticleListComponent,
  }
  ,
  {
    path:'AddArticle',
    pathMatch:'full',
    component:ArticleFormComponent,
  },
  {
    path:'article/:id/edit',
    pathMatch:'full',
    component: ArticleFormComponent,
  },
  {
    path:'event',
    pathMatch:'full',
    component:EventListComponent,
  }
  ,
  {
    path:'Addevent',
    pathMatch:'full',
    component:EventFormComponent,
  },
  {
    path:'event/:id/edit',
    pathMatch:'full',
    component: EventFormComponent,
  }
  ,
  {
    path: 'login',  pathMatch:'full', component: LogInComponent
  },
  {
    path: 'app-forbidden',  pathMatch:'full', component: ForbiddenComponent},

  {
    path:'**',
    redirectTo:'Dashboard',
  },


]

@NgModule({
  declarations: [],
  imports: [ RouterModule.forRoot(routes)
  ]
})
export class AppRoutingModule { }

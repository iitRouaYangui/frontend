import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {Outil} from "../../Models/Outil";
import {ToolsService} from "../../Services/tools.service";
import {DialogBoxComponent} from "../dialog-box/dialog-box.component";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../../Services/auth.service";

@Component({
  selector: 'app-tools-list',
  templateUrl: './tools-list.component.html',
  styleUrls: ['./tools-list.component.css']
})
export class ToolsListComponent implements OnInit {
  displayedColumns: string[] = ['namesource' ,'source', 'date','membre', 'action'];
  list:any;
  dataSource:MatTableDataSource<Outil>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private TS : ToolsService ,private router: Router, public dialog: MatDialog , private http: HttpClient , public authService : AuthService) {
    this.dataSource = new MatTableDataSource(this.list);
  }
  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  delete(id: string) {
    const dialogRef = this.dialog.open(DialogBoxComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(id);
        this.TS.remove(id).then(() => this.getOutils());

      }
    });
  }

  getOutils()
  {
     this.TS.getAllOutils().then((data)=>{
       this.list=data;
       this.dataSource.data=this.list;
     });
  }

  ngOnInit(): void {

    this.getOutils();
  }

}

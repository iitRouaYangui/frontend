import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ArticleService} from "../../Services/article.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Article} from "../../Models/Article";
import { DatePipe } from '@angular/common'
@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.css']
})
export class ArticleFormComponent implements OnInit {
  formArticle: any;
  @ViewChild('fileInput') fileInput!: ElementRef;
  fileAttr = 'Choose your article';
  pdfFile: any;
  pdfSrc: any;
  pdfBufferRender: any;
  localPDF:any;
  currentId:any;
  item1:any;
  type:any;
  titre:any;
  lien:any;
  sourcepdf:any;
  dateMaxDate = new Date();
  constructor(private articleService: ArticleService, private router: Router , private activatedRoute:ActivatedRoute,public datepipe: DatePipe) {

  }
  initForm(item:any): void {
    if(item?.date!=null){
      this.formArticle = new FormGroup({
        type: new FormControl(item.type, [Validators.required]),
        titre: new FormControl(item.titre, [Validators.required]),
        lien: new FormControl(item.lien,[Validators.required] ),
        date: new FormControl(item.date, [Validators.required]),
        sourcepdf: new FormControl(null, [Validators.required]),
      })
    }
    else
    {
      this.formArticle = new FormGroup({
        type: new FormControl(null, [Validators.required]),
        titre: new FormControl(null, [Validators.required]),
        lien: new FormControl(null,[Validators.required] ),
        date: new FormControl(new Date(), [Validators.required]),
        sourcepdf: new FormControl(null, [Validators.required]),
      })
    }

  }


  ONSUB(): void {
    const articleToSave : Article  ={...this.item1 , ...this.formArticle.value};
    articleToSave.sourcepdf=this.localPDF;
    this.articleService.saveArticle(articleToSave);
  }
  uploadFileEvt(imgFile: any) {
    const pdfTatget: any = imgFile.target;
    this.fileAttr = '';
      Array.from(imgFile.target.files).forEach((file: any) => {
        this.fileAttr += file.name;
      });
    if (typeof FileReader !== 'undefined') {
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.pdfSrc = e.target.result;
        this.localPDF = this.pdfSrc;
        console.log( this.pdfSrc);
      };
      this.pdfBufferRender = pdfTatget.files[0];
      reader.readAsDataURL(pdfTatget.files[0]) ;

    }
  }


  ngOnInit(): void {
    this.currentId=this.activatedRoute.snapshot.params['id'];
    if (!!this.currentId) {
      console.log(this.currentId);
      this.articleService.getArticlebyId(this.currentId).then((item: Article) => {

        this.item1 = item;
        this.initForm(this.item1)
        console.log(this.item1)
      });

    }else {
      this.initForm(null);
    }


  }

}

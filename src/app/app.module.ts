import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatTableModule} from '@angular/material/table';
import { AppComponent } from './app.component';
import { MemberListComponent } from './member-list/member-list.component';
import { AppRoutingModule } from './app-routing.module';
import {RouterModule} from "@angular/router";
import { MemberFormComponent } from './member-form/member-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {FlexLayoutModule} from "@angular/flex-layout";
import {MaterialModule} from "./material.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {SidebarComponent} from "./sidebar/sidebar.component";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {DialogBoxComponent} from "./dialog-box/dialog-box.component";
import {MatDialogModule} from "@angular/material/dialog";
import {ToolsListComponent} from "./tools-list/tools-list.component";
import  {MatDatepickerModule} from '@angular/material/datepicker';
import {MAT_DATE_LOCALE, MatNativeDateModule} from '@angular/material/core';
import {ToolsFormComponent} from "./tools-form/tools-form.component";
import {MatInputModule} from '@angular/material/input';
import {ArticleFormComponent} from "./article-form/article-form.component";
import {ArticleListComponent} from "./article-list/article-list.component";
import {EventListComponent} from "./event-list/event-list.component";
import {EventFormComponent} from "./event-form/event-form.component";
import {ToastrModule} from "ngx-toastr";
import {DatePipe} from "@angular/common";
import {ForbiddenComponent} from "./forbidden/forbidden.component";
import { LogInComponent } from './log-in/log-in.component';

@NgModule({
    declarations: [
        AppComponent,
        MemberListComponent,
        MemberFormComponent,
        SidebarComponent,
        DashboardComponent, DialogBoxComponent, ToolsListComponent, ToolsFormComponent,
        ArticleFormComponent, ArticleListComponent, EventListComponent, EventFormComponent, ForbiddenComponent, LogInComponent,

    ],
  imports: [
    BrowserModule,
    HttpClientModule,
    MatTableModule,
    AppRoutingModule,
    RouterModule,
    BrowserAnimationsModule,
    FlexLayoutModule,FormsModule,ReactiveFormsModule,
    MaterialModule , HttpClientModule, MatDialogModule,
    MatDatepickerModule ,MatNativeDateModule,MatInputModule,ToastrModule.forRoot(), BrowserAnimationsModule
  ],
  providers: [
    MatDatepickerModule,
    MatNativeDateModule,MatInputModule,DatePipe,
    {provide: MAT_DATE_LOCALE, useValue: 'en-GB'},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

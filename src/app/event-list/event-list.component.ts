import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material/dialog";
import {DialogBoxComponent} from "../dialog-box/dialog-box.component";
import {Evenement} from "../../Models/Evenement";
import {EventService} from "../../Services/event.service";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../../Services/auth.service";

@Component({
  selector: 'app-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.css']
})
export class EventListComponent implements OnInit {

  liste : any;
  displayedColumns: string[] = ['title', 'date', 'lieu', 'member', 'action'];
  dataSource: any;

  /*test:any ={
    nameMember : localStorage.getItem("loggedUser"),
    idMember : localStorage.getItem("loggedId"),};*/


  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private ES: EventService, private router: Router, public dialog: MatDialog, public http: HttpClient , public authService : AuthService) {
    this.dataSource = new MatTableDataSource(this.liste);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  delete(id: string) {
    const dialogRef = this.dialog.open(DialogBoxComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.ES.remove(id).then(() => this.getEvents());

      }
    });
  }


 /* getEvents() {
    this.http.get('http://localhost:9000/labo/api/events').subscribe(data => {
      this.liste = data;
      console.log(data);
      console.log(this.liste.forEach((c: any)=>console.log(c.membre.id)));
      console.log(this.test.nameMember);
      console.log(this.test.idMember);

    }, error => { console.log(error.messages) ; } ) ;
  }*/
  getEvents (){
    this.ES.getEvents().then((data)=> {
    this.liste= data;
    this.dataSource.data=this.liste;})
  }

  ngOnInit(): void {
    //this.fetchDataSource();
    this.getEvents();

  }

}

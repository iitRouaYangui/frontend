import {Injectable} from '@angular/core';
import {Member} from "../Models/Member";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  loggedUser: any;
  loggedCin:any;
  isloggedIn: Boolean = false;
  role: any;
  apiURL: string = 'http://localhost:9000/labo/api/login';


  constructor(private router: Router, private http: HttpClient) {
  }
  getUserFromDB(cin:string):Observable<Member>
  {
    const url = `${this.apiURL}/${cin}`;
    return this.http.get<Member>(url);
  }

  SignIn(user :Member){
    this.loggedUser = user.nom;
    this.isloggedIn = true;
    this.role = user.role;
    localStorage.setItem('loggedUser',this.loggedUser);
    localStorage.setItem('loggedCin',user.cin);
    localStorage.setItem('loggedId',user.id);
    localStorage.setItem('isloggedIn',String(this.isloggedIn));
  }

  logout() {
    this.isloggedIn= false;
    this.loggedUser = undefined;
    this.role = undefined;
    localStorage.removeItem('loggedUser');
    localStorage.setItem('isloggedIn',String(this.isloggedIn));
    this.router.navigate(['/login']).then(()=>window.location.reload());
  }

  isAdmin():Boolean{
    if (this.role == 'admin')
      return true;
    else
      return false ;


  }

  username():string|null{
    return localStorage.getItem('loggedCin');
  }
  getUserRoles(cin :string){
    this.getUserFromDB(cin).subscribe((user: Member)=>{
      this.role = user.role;
    });
  }
  setLoggedUserFromLocalStorage(cin : string , nom :string) {
    this.loggedUser=nom;
    this.loggedCin = cin;
    this.isloggedIn = true;
    this.getUserRoles(cin);
  }
}

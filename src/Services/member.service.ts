import {Injectable, OnInit} from '@angular/core';
import {GLOBAL1} from "../app/app_config";
import {Member} from "../Models/Member";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import { DatePipe } from '@angular/common'
import {Outil} from "../Models/Outil";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class MemberService implements OnInit {

  constructor(private httpClient: HttpClient ,public datepipe: DatePipe , private toastr: ToastrService,private route: Router) {
  }

  saveMember(member: Member): void {


   const memberToSave = {...member,};
    if(memberToSave?.id != null)
    {
       this.httpClient.put<Member>('http://localhost:9000/labo/api/updateMember/'+memberToSave.id ,memberToSave).toPromise().then(() => this.route.navigate(['./members']));
    }
    else
    {

      memberToSave.createddate = new Date();

      this.httpClient.post<Member>('http://localhost:9000/labo/api/addMember',memberToSave).toPromise().catch((err: HttpErrorResponse) => {
        if(err.status==409)
        {
          this.toastr.error('CIN all ready exist tcheck your table !', 'Error');

        }
      }).then(() => this.route.navigate(['./members']));
    }
  }

  getMemberbyId(id: string|null): Promise<any> {
    return  this.httpClient.get<Member>('http://localhost:9000/labo/api/member/'+id).toPromise();
  }

  getMemberlazybyId(id: string|null): Promise<any> {
    return  this.httpClient.get<Member>('http://localhost:9000/labo/api/memberlazy/'+id).toPromise();
  }


  remove(id: string): Promise<void> {
    return this.httpClient.delete<void>('http://localhost:9000/labo/api/membres/' + id).toPromise();
  }

  getAllMembers():Promise<Member[] | undefined>{
    return this.httpClient.get<Member[]>('http://localhost:9000/labo/api/membersNotRoleIn?role=admin').toPromise();
  }
  ngOnInit(): void {

  }
}
